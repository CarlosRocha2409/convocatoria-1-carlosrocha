﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Convocatoria_1
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void extinguidoresToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmGestionExtinguidor fge = new FrmGestionExtinguidor();
            fge.MdiParent = this;
            fge.DsExtinguidor = dsExrtinguidor;
            fge.Show();


        }

        private void clientesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GestionCliente gc = new GestionCliente();
            gc.MdiParent = this;
            gc.DsCliente = dsExrtinguidor;
            gc.Show();
        }
    }
}
