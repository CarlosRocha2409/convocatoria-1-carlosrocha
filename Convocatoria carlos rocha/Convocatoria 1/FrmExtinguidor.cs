﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Convocatoria_1
{
    public partial class FrmExtinguidor : Form
    {
        DataSet dsExtinguidor;
        BindingSource bsExtinguidor;
        private DataTable tblExtinguidor;

        public DataSet DsExtinguidor
        {
            get
            {
                return dsExtinguidor;
            }

            set
            {
                dsExtinguidor = value;
            }
        }

        public DataTable TblExtinguidor
        {
            get
            {
                return tblExtinguidor;
            }

            set
            {
                tblExtinguidor = value;
            }
        }

        public FrmExtinguidor()
        {
            InitializeComponent();
            bsExtinguidor = new BindingSource();
        }

        private void FrmExtinguidor_Load(object sender, EventArgs e)
        {
            bsExtinguidor.DataSource = DsExtinguidor;
            bsExtinguidor.DataMember = DsExtinguidor.Tables["Extinguidor"].TableName;
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            //categoría, tipo_extinguidor, marca, capacidad, unidad_medida, lugar_designado, fecha_recarga 
            String Categoria = cmbCategoria.SelectedItem.ToString();
            String Tipo= cmbTipo.SelectedItem.ToString();
            String Marca= cmbMarca.SelectedItem.ToString();
            String Capacidad = cmbCapacidad.SelectedItem.ToString();
            String UnidadM= cmbUMedida.SelectedItem.ToString();
            String Lugar = txtLugar.Text;
            DateTime Fecha = DateTime.Now;

            TblExtinguidor.Rows.Add(tblExtinguidor.Rows.Count + 1,  Categoria, Tipo, Marca, UnidadM, Lugar, Fecha);
            Dispose();

        }
    }
}
