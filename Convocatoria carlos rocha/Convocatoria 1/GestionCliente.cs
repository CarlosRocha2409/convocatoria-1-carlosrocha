﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Convocatoria_1
{
    public partial class GestionCliente : Form
    {
        DataSet dsCliente;
        BindingSource bsCliente;

        public DataSet DsCliente
        {
            get
            {
                return dsCliente;
            }

            set
            {
                dsCliente = value;
            }
        }
        public GestionCliente()
        {
            InitializeComponent();
            bsCliente = new BindingSource();

        }

        private void GestionCliente_Load(object sender, EventArgs e)
        {
            bsCliente.DataSource = DsCliente;
            bsCliente.DataMember = DsCliente.Tables["Cliente"].TableName;
            dataGridView1.DataSource = bsCliente;
            dataGridView1.AutoGenerateColumns = true;
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            FrmCliente fc = new FrmCliente();
            fc.DsCliente = DsCliente;
            fc.TblCliente = DsCliente.Tables["Cliente"];
            fc.ShowDialog();
        }
    }
}
