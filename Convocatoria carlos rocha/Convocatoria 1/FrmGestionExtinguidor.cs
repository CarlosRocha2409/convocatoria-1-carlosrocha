﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Convocatoria_1
{
    public partial class FrmGestionExtinguidor : Form
    {
        DataSet dsExtinguidor;
        BindingSource bsExtinguidor;

        public DataSet DsExtinguidor
        {
            get
            {
                return dsExtinguidor;
            }

            set
            {
                dsExtinguidor = value;
            }
        }

        public FrmGestionExtinguidor()
        {
            InitializeComponent();
            bsExtinguidor = new BindingSource();

        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            FrmExtinguidor fe = new FrmExtinguidor();
            fe.DsExtinguidor = DsExtinguidor;
            fe.TblExtinguidor = DsExtinguidor.Tables["Extinguidor"];
            fe.ShowDialog();
        }

        private void FrmGestionExtinguidor_Load(object sender, EventArgs e)
        {
            bsExtinguidor.DataSource = DsExtinguidor;
            bsExtinguidor.DataMember = DsExtinguidor.Tables["Extinguidor"].TableName;
            dataGridView1.DataSource = bsExtinguidor;
            dataGridView1.AutoGenerateColumns = true;
        }
    }
}
