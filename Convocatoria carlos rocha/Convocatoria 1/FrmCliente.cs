﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Convocatoria_1
{
    public partial class FrmCliente : Form
    {
        DataSet dsCliente;
        BindingSource bsCliente;
        private DataTable tblCliente;

        public DataSet DsCliente
        {
            get
            {
                return dsCliente;
            }

            set
            {
                dsCliente = value;
            }
        }

        public DataTable TblCliente
        {
            get
            {
                return tblCliente;
            }

            set
            {
                tblCliente = value;
            }
        }

        public FrmCliente()
        {
            InitializeComponent();
            bsCliente = new BindingSource();

        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            String Cedula = msktxtCedula.Text;
            String Nombre = txtNombre.Text;
            String Apellido = txtApellido.Text;
            String Celular = msktxtCelular.Text;
            String Correo = txtCorreo.Text;
            String dir = txtDireccion.Text;
            String mun = txtMunicipio.Text;
            String dep = txtDepartamento.Text;
            tblCliente.Rows.Add(tblCliente.Rows.Count + 1,Cedula, Nombre,Apellido,Celular,Correo,dir,mun,dep);
            Dispose();
    
        }

        private void FrmCliente_Load(object sender, EventArgs e)
        {
            bsCliente.DataSource = DsCliente;
            bsCliente.DataMember = DsCliente.Tables["Cliente"].TableName;
        }
    }
}
