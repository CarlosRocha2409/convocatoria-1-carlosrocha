﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Convocatoria_1.Entities
{
    class Exinguidor
    {
        int id;
        String categoria;
        String tipo, marca, lugar_designado;
        int unidad_medida,capacidad;
        DateTime fecha_recarga;

        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public string Categoria
        {
            get
            {
                return categoria;
            }

            set
            {
                categoria = value;
            }
        }

        public string Tipo
        {
            get
            {
                return tipo;
            }

            set
            {
                tipo = value;
            }
        }

        public string Marca
        {
            get
            {
                return marca;
            }

            set
            {
                marca = value;
            }
        }

        public string Lugar_designado
        {
            get
            {
                return lugar_designado;
            }

            set
            {
                lugar_designado = value;
            }
        }

        public int Unidad_medida
        {
            get
            {
                return unidad_medida;
            }

            set
            {
                unidad_medida = value;
            }
        }

        public int Capacidad
        {
            get
            {
                return capacidad;
            }

            set
            {
                capacidad = value;
            }
        }

        public DateTime Fecha_recarga
        {
            get
            {
                return fecha_recarga;
            }

            set
            {
                fecha_recarga = value;
            }
        }

        public Exinguidor(int id, string categoria, string tipo, string marca, string lugar_designado, int unidad_medida, int capacidad, DateTime fecha_recarga)
        {
            this.id = id;
            this.categoria = categoria;
            this.tipo = tipo;
            this.marca = marca;
            this.lugar_designado = lugar_designado;
            this.unidad_medida = unidad_medida;
            this.capacidad = capacidad;
            this.fecha_recarga = fecha_recarga;
        }





        //id, categoría, tipo_extinguidor, marca, capacidad, unidad_medida, lugar_designado, fecha_recarga
    }
}
